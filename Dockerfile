from fedora
workdir /build
run dnf install -y curl
run dnf install -y skopeo
run dnf install -y 'dnf-command(copr)'
run dnf copr enable -y ganto/umoci
run dnf install -y umoci
run dnf install -y qemu-img
run dnf install -y fuse fuse-libs
run dnf install -y e2fsprogs
copy . .
cmd ./convert-docker-to-qcow ubuntu
